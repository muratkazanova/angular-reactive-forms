import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeFormComponent } from './Components/employee-form/employee-form.component';
import { SharedModule } from '../shared/shared.module';
import { EmployeeFormReactiveComponent } from './Components/employee-form-reactive/employee-form-reactive.component';

@NgModule({
  declarations: [EmployeeFormComponent, EmployeeFormReactiveComponent],
  imports: [CommonModule, SharedModule, EmployeeRoutingModule]
})
export class EmployeeModule {}
