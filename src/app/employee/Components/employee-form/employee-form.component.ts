import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {
  constructor() {}
  employee = {
    firstname: '',
    gender: 'male',
    department: 'human resources'
  };
  departments: any[] = [];

  ngOnInit() {
    this.departments = [
      { value: 'human resources', text: 'Human Resources' },
      { value: 'information technology', text: 'IT' }
    ];
  }
  onkeydown(event: KeyboardEvent) {
    if (event.keyCode == 13) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  onSubmit(value: any) {
    alert(JSON.stringify(value));
  }
}
