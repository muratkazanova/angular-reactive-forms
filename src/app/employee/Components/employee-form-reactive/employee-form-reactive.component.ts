import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-employee-form-reactive',
  templateUrl: './employee-form-reactive.component.html',
  styleUrls: ['./employee-form-reactive.component.scss']
})
export class EmployeeFormReactiveComponent implements OnInit {
  employeeForm: FormGroup;
  notificationMethods: { type: string; checked: boolean }[] = [];
  constructor(private fb: FormBuilder) {}

  get emailaddress() {
    return this.employeeForm.get('emailaddress');
  }

  get phonenumber() {
    return this.employeeForm.get('phonenumber');
  }
  get notificationmethods() {
    return this.employeeForm.get('notificationmethods');
  }

  get languages() {
    return this.employeeForm.get('languages');
  }

  get certificates(): FormArray {
    return <FormArray>this.employeeForm.get('certificates');
  }

  ngOnInit() {
    this.employeeForm = this.fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      gender: 'male',
      languages: '',
      age: [18, [Validators.required, Validators.min(18), Validators.max(99)]],
      department: ['', Validators.required],
      phonetype: 'home',
      phonenumber: '',
      notificationmethods: ['', [Validators.required]],
      emailaddress: ['', [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      certificates: this.fb.array([])
    });
  }

  onPatch() {
    this.employeeForm.patchValue({
      languages: ['english', 'turkish'],
      firstname: 'Ertan'
    });
  }

  updateNotificationMethod(event: any) {
    const notificationMethod = event.target as HTMLInputElement;
    this.updateNotificationMethodsCollection({
      type: notificationMethod.value,
      checked: notificationMethod.checked
    });
  }

  updateNotificationMethodsCollection(method: {
    type: string;
    checked: boolean;
  }) {
    const nm = this.notificationMethods.find(e => e.type === method.type);
    if (nm) {
      nm.checked = method.checked;
    } else {
      this.notificationMethods.push(method);
    }

    if (this.notificationMethods.filter(m => m.checked).length > 0) {
      this.notificationmethods.setValue('valid');
    } else {
      this.notificationmethods.setValue('');
    }
    this.notificationmethods.updateValueAndValidity();

    console.log(this.notificationMethods);
    this.setNotificationValidators();
  }

  setNotificationValidators() {
    const selectedMehtods = this.notificationMethods.filter(m => m.checked);
    if (selectedMehtods) {
      const emailMethod = selectedMehtods.find(m => m.type === 'email');
      const phoneOrTextMethod = selectedMehtods.find(
        m => m.type === 'phone' || m.type === 'text'
      );

      if (emailMethod && emailMethod.checked) {
        this.emailaddress.setValidators([
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')
        ]);
      } else {
        this.emailaddress.setValidators([
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')
        ]);
      }

      if (phoneOrTextMethod && phoneOrTextMethod.checked) {
        this.phonenumber.setValidators(Validators.required);
      } else {
        this.phonenumber.setValidators(null);
      }
      this.emailaddress.updateValueAndValidity();
      this.phonenumber.updateValueAndValidity();
    }
  }

  buildCertificateGroup(): FormGroup {
    return this.fb.group({
      certificateName: ['', [Validators.required]],
      certificateDate: ['', [Validators.required]]
    });
  }

  addCertificate() {
    this.certificates.push(this.buildCertificateGroup());
  }

  onSubmit() {}
}
