import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeFormComponent } from './Components/employee-form/employee-form.component';
import { EmployeeFormReactiveComponent } from './Components/employee-form-reactive/employee-form-reactive.component';

const routes: Routes = [
  { path: 'employeeForm', component: EmployeeFormComponent },
  { path: 'employeeFormReactive', component: EmployeeFormReactiveComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
