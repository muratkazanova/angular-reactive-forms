import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NameFormComponent } from '../Shared/Components/name-form/name-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MkssNumberComponent } from '../Shared/Components/mkss-number/mkss-number.component';
import { MultipleMinDirective } from './Directives/multiple-min.directive';

@NgModule({
  declarations: [NameFormComponent, MkssNumberComponent, MultipleMinDirective],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    NameFormComponent,
    MkssNumberComponent,
    MultipleMinDirective
  ]
})
export class SharedModule {}
