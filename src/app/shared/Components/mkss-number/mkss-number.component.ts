import {
  Component,
  OnInit,
  OnChanges,
  forwardRef,
  Input,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
@Component({
  selector: 'mkss-number',
  templateUrl: './mkss-number.component.html',
  styleUrls: ['./mkss-number.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MkssNumberComponent),
      multi: true
    }
  ]
})
export class MkssNumberComponent
  implements OnInit, OnChanges, ControlValueAccessor {
  value: number;
  @Input() min = 0;
  @Input() max = 100;
  @Input() step;
  @ViewChild('numvalue') numfield;
  constructor() {}
  ngOnChanges() {
    this.step = (this.step && +this.step) || 1;
  }
  onModelChange: Function;
  onTouched: Function;
  writeValue(value): void {
    this.value = +value || 0;
  }
  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit() {}
  increment() {
    if (this.value < this.max) {
      this.value += this.step;
      this.onModelChange(this.value);
    }
  }
  decrement() {
    if (this.value > this.min) {
      this.value -= this.step;
      this.onModelChange(this.value);
    }
  }

  onkeydown(event: KeyboardEvent) {
    const handlers = {
      ArrowUp: () => this.increment(),
      ArrowDown: () => this.decrement()
    };

    if (handlers[event.key]) {
      handlers[event.key]();
      event.preventDefault();
      event.stopPropagation();
    }
  }
}
