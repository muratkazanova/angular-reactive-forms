import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { NgOnChangesFeature } from '@angular/core/src/render3';

@Component({
  selector: 'name-form',
  templateUrl: './name-form.component.html',
  styleUrls: ['./name-form.component.scss']
})
export class NameFormComponent implements OnInit {
  @Input() parent: FormGroup;
  @Input() fgn: string;
  get firstname() {
    return this.parent.get(this.fgn).get('firstname');
  }
  get lastname() {
    return this.parent.get(this.fgn).get('lastname');
  }
  constructor() {}
  ngOnInit() {}
}
