import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
@Directive({
  selector: '[MultipleMin]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MultipleMinDirective,
      multi: true
    }
  ]
})
export class MultipleMinDirective implements Validator {
  @Input() MultipleMin: number;
  constructor() {}
  // { MultipleMin : true}
  validate(control: AbstractControl): { [key: string]: any } | null {
    const sl = control.value
      ? (control.value as Array<any>).filter(v => v !== '').length
      : 0;
    if (sl >= this.MultipleMin) {
      return null;
    } else {
      return { MultipleMin: true };
    }
  }
}
